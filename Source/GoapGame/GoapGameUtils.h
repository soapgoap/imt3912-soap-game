// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GoapGameUtils.generated.h"

/**
 * 
 */
UCLASS()
class GOAPGAME_API UGoapGameUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Utils|Sort")
	static TArray<AActor*> SortActorsByDistance(TArray<AActor*> TargetActors, AActor* FromActor);
	UFUNCTION(BlueprintCallable, Category = "Utils|Sort")
	static AActor* GetClosestActor(TArray<AActor*> TargetActors, AActor* FromActor);

};
