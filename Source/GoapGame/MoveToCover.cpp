// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapGame.h"
#include "MoveToCover.h"



TArray<AActor*> UMoveToCover::SortActorsByDistance(TArray<AActor*> TargetActors, AActor* FromActor)
{
	TargetActors.Sort([FromActor](const AActor& lhs, const AActor& rhs) {return lhs.GetDistanceTo(FromActor) < rhs.GetDistanceTo(FromActor); });
	return TargetActors;
}