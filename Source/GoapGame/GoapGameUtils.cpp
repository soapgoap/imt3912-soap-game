// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapGame.h"
#include "GoapGameUtils.h"



TArray<AActor*> UGoapGameUtils::SortActorsByDistance(TArray<AActor*> TargetActors, AActor* FromActor)
{
	TargetActors.Sort([FromActor](const AActor& lhs, const AActor& rhs) {return lhs.GetDistanceTo(FromActor) < rhs.GetDistanceTo(FromActor); });
	return TargetActors;
}

AActor* UGoapGameUtils::GetClosestActor(TArray<AActor*> TargetActors, AActor * FromActor)
{
	AActor* ClosestActor = nullptr;
	float CurrentBestDistance = 1000000.f;
	for (auto& Target : TargetActors)
	{
		float Distance = Target->GetDistanceTo(FromActor);
		if (Distance < CurrentBestDistance)
		{
			CurrentBestDistance = Distance;
			ClosestActor = Target;
		}
	}
	return ClosestActor;
}
