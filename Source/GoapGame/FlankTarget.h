// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapActionBase.h"
#include "Algo/Sort.h"
#include "FlankTarget.generated.h"

/**
 * 
 */
UCLASS()
class GOAPGAME_API UFlankTarget : public UGoapActionBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "GOAP|Action|FlankTarget")
	TArray<AActor*> SortActorsByDistance(TArray<AActor*> TargetActors, AActor* FromActor);
	
};
