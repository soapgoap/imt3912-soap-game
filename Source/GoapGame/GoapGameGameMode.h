// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "GoapGameGameMode.generated.h"

UCLASS(minimalapi)
class AGoapGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGoapGameGameMode();
};



