// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "GoapAIController.h"
#include "ComplexAgentController.generated.h"

/**
 * 
 */
UCLASS()
class GOAPGAME_API AComplexAgentController : public AGoapAIController
{
	GENERATED_BODY()
	
public:
	AComplexAgentController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
private:
	bool bPaused;
public:
	UFUNCTION(BlueprintCallable, Category = "GOAP")
	void Pause();
	UFUNCTION(BlueprintCallable, Category = "GOAP")
	void Resume();
};
