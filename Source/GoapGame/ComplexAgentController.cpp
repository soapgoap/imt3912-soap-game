// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapGame.h"
#include "ComplexAgentController.h"

AComplexAgentController::AComplexAgentController()
{
	//UE_LOG(LogTemp, Warning, TEXT("Complex agent constructor"));
	bPaused = true;
}

void AComplexAgentController::BeginPlay()
{
	//UE_LOG(LogTemp, Warning, TEXT("Complex agent beginplay"));
	Super::BeginPlay();
}

void AComplexAgentController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AComplexAgentController::Pause()
{
	UE_LOG(LogTemp, Warning, TEXT("Paused."));
	bPaused = true;
}

void AComplexAgentController::Resume()
{
	bPaused = false;
}
