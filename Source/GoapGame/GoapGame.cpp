// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GoapGame.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GoapGame, "GoapGame" );

DEFINE_LOG_CATEGORY(LogGoapGame)
 