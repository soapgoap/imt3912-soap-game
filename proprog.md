This project was developed by Jonas Askeland and Sveinung T�rresdal. We used two repos, [IMT3912 - SOAP Game](https://bitbucket.org/soapgoap/imt3912-soap-game) and [IMT3912 - SOAP](https://bitbucket.org/soapgoap/imt3912-soap). SOAP was the first repo, where we started developing the prototype, the UI and the plugin packaging. The SOAP Game repo was our test to see that the packaged plugin worked and where we developed the demonstration.
 
The Professional Programming course inspired us to write about our experiences with developing in the Unreal engine and our development process in the bachelor thesis paper. You can read more about our experiences notably in chapter 10: Discussion. Because of that the text you see here will mostly be reiterating things we have already discussed in the bachelor paper. 
 
#Strenghts and weaknesses of languages you used in your project
Our code was written in C++ and Blueprints. We also had to use Slate in our C++ when trying to develop UI in the editor. Some configuration files were C# files, but we did not write any C# code.
 
[Link to coding conventions and standards. ](https://docs.unrealengine.com/latest/INT/Programming/Development/CodingStandard/)
 
## Blueprints
Within the engine�s own editor is a toolkit for creating *blueprints*, which is a visual scripting language designed by Epic. It was originally developed under the title �k2�, so there are some things still named under that convention.
 
Blueprints form an easy path to coding in Unreal Engine 4. It lets you bypass a lot of the headaches presented in C++, such as getting references to objects normally not in your included files. The system also has its own garbage collection. All in all, it�s quite nice and tidy to work with.
 
That said, it is a system written in C++, and we�ve discovered a number of strange cases with it where it doesn�t behave as intended. This was notable with TMaps, which weren�t supported at all at first, and only got some support in its later version. There were also bugs like the inheritance issues when using virtual functions. 
 
## C++
Unreal has developed a ["reflection system"](https://www.unrealengine.com/en-US/blog/unreal-property-system-reflection) for use in their C++ code. This system gives us as developers tools to allow the program to examine itself at runtime, which is not natively supported in C++. For us this primarily shows up in automated memory management and communication between C++ and Blueprints.
 
Unreal also provides a series of smart pointers, much like the standard library smart pointers. Unreal advises that we use their pointers instead of the std library pointers because then Unreal can guarantee that they work on all platforms that Unreal supports. 
 
We used std shared pointers at one point in our code before we came across Unreals own implementation, but have yet to go back and replace our use of std pointers with Unreal pointers. It is a low priority issue for us since we are only developing on the PC at the moment, but we will have to change it in the future. 
 
### Slate
Slate is a sort of markdown language used to describe and define UI layouts that can appear in the editor (and also at game runtime). It is written alongside your C++ code in your .cpp files. 
 
We have written about our troubles with slate in our bachelor paper, instead of reiterating that here I suggest you read over chapter 5 and 10  in the paper. 
 
 
#Process and communication systems
We decided early on that we did not want to use Jira or other larger project management tools because we were just a group of 2. Additionally we were both working out of the same office in order to have a dedicated workspace. This allowed us to communicate face to face whenever we needed, usually prompted by sending a message to the other through the chat client Discord. 
 
Other than that we kept track of our development progress by opening and closing issues in bitbucket. 
 
#Use of version control systems, ticket tracking, branching, version control 
We kept our configuration fairly simple to avoid overhead. Notably, we were not using branching, as we were already having issues with Unreal Engine 4 and its binarized proprietary file format: .uasset.
 
The biggest �version control� we did was creating a new project for our final phase, which warranted us creating a second repo - we attempted to use a branch for it, but we found the repo was not very cooperative about the way we wanted to do it.
 
#Use of libraries and integration of libraries 
A useful aspect of developing with the Unreal engine is that we have full source code access and can rebuild the engine (and the editor and all the development tools) ourselves. This allows us to extend the engine however we please (unless constrained by EULA), meaning we could link libraries and use them in our project. 
 
The Unreal engine makes heavy use of dynamic linking. We would continuously rebuild our code during development without needing to rebuild the entire engine. We would build our code while the editor was running and have it reload our code (now compiled to libraries) at runtime, a feature they call �hot reload�. 
 
We did not have to link any outside libraries because the Unreal engine is a mature game development platform that provided us with a large array of tools and functionality out of the box. 
 
If we want additional functionality, we might find them as plugins in the Unreal marketplace. These would function much the same as normal libraries, except they could contain more than just code (such as graphics assets). 
 
We packaged parts of our own work into such a plugin to make GOAP easy to integrate into another project. Much the same as we could have done with libraries if we had not developed specifically for the Unreal engine. 
 
You can also link libraries in plugins, skipping the step of needing to rebuild the engine but instead just rebuilding the plugins. 
 
#Professionalism in your approach to software development 
 
We were a two-man team, and so while we were interested in many of the techniques on display, such as semantic commits, user stories and more, we were more than anything worried about the overhead that they would impose on our small team.
 
Of importance to us however was the fact that the library we were building was intended for public use, and so we were working to maintain descriptive code that followed the conventions laid out by the Epic team.
 
 
 
#Use of code review:
 
We were the first group to opt for a code review, but we did not do any more reviews afterwards. The review had some issues, firstly because we ended up with showing the code from our laptop instead of having Mariusz go through it himself. I can't really recall why we had to do it like that. Secondly, we started off by explaining what the code was for, but then we kept explaining what the code did and we walked through it while explaining it. Mariusz did not interrupt us, so we ended up explaining the entire code instead of having him read it and comment on it.
 
At the end we got some comments on the coding conventions and style, such as our choice of camelcasing. When we explained that we used the coding convention defined for development on the Unreal engine Mariusz agreed with it saying that it was good to try to conform to community expectations. Another comment was on a function name along the lines of "GetValidPlan". By having the word "valid" in the name, it implied that there was such a thing as an "invalid" plan, but we did not make use of any "invalid" plans and that could make the function name confusing. The heavy verbosity of the code was commented on as being a good thing, making the code easier to read. We had split a large function into multiple smaller functions where it seemed logical for us to do so and this prompted a comment about the length of functions.
 
There was a comment from the audience regarding A*. We had said we were using A*, but he said he had not been able to follow it properly and suggested we should separate it out logically or comment it accordingly so it would be easy to follow our implementation if you were already familiar with the A* algorithm.
 
In the end the review gave us validation that the coding convention used for Unreal was a good one, and that we were effectively conforming to it. However we did not get any comments on any other aspects of the code, probably because of how we read the code to them instead of having Mariusz pick through it himself.
 
We could probably have benefitted from another code review, but the code we had presented was the largest and most complex piece of our work already. The only other code that would be significantly large enough to warrant a proper review would have been the UI code. Difficulties with slate however made it so we never managed to develop that code. And a sizable portion of our work was done using Blueprints from that point on which we think would be difficult to review.
 
 
#Personal reflection
 
## Jonas
Professional programming as a course has probably been the most interesting course we have had. It codified a lot of the good opinions and attitudes we were taught about programming over the years. And code reviews seem like they are a nice way to get back to reality when you have been too deep in your code for too long. 
 
I think one of the shortcomings of this bachelor has been a lack of real world experience, such as internships. This course felt like the closest we would get to being exposed to the culture of a real software development company, and I am thankful for that and hope I will encounter a lot of the same good attitudes that were discussed. 
 
Even though we attempted to be �professional� about our development we had shortcomings that, looking back at it, makes the project feel like a hobby project. Primarily the absence of proper rigid testing and relaxed attitudes towards process management did not strike me as being at home in a professional setting. Even when we had decent reason to avoid the overhead. 
 
 
 
## Sveinung
 
For me personally, professional programming is something I think should be in our blood - metaphorically that is. We should all be striving to make our work easily understood by others, both for our own sake (coming back to it later), or for when we need others to work on what we�ve done.
 
Towards this end, the most obvious thing I think to me is that the code needs to be �self-documenting� - not in a doxygen fashion, but that functions do their best to explain what their function is in their name, that functions are not butchered up pieces, and that variables explain what they exist for.
 
What I mean by butchered up pieces is, for example, a function that contains several function calls, which use function calls of their own, which use function calls, etc. If it�s a rabbit hole, the odds of things becoming non-obvious go up fast. A longer section of code that explains itself well is better than reducing the size of the function by exporting the code into separate, non-obvious functions.
 
However, creating a meaningful paper-trail in commits also has a lot of meaning to it. It was not very necessary for us on this project, so we skipped out on it for our project, but we were quite fond of features such as semantic commits.
 
For my part, this course has been pretty valuable in seeing a side of development I hadn�t really invested much thought into before, and I can definitely see how the subjects we�ve gone over throughout it are very valuable for especially larger teams, where communication between individuals will not suffice or be difficult, and instead automated solutions are necessary for handling the project.
