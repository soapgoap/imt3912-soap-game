# SOAP #
"GOAP, rhymes with soap" - This is among the opening words from Jeff Orkin as he elaborates on his (then) new AI paradigm.

SOAP, or Special Operations Action Planning, is the bachelor's thesis project for Jonas Askeland and Sveinung Tørresdal. Our name comes from Jeff's quote, combined with the fact that we want to create a videogame of the same title - in the classic tactical shooter genre ala SWAT and such. Essentially, the name is an homage to SWAT, and taking Jeff's rhyme under our wing in doing so.

Our project entails creating a functioning GOAP algorithm within Unreal Engine 4. The ultimate goal is to release a functional tool with which you can implement GOAP with relative ease, and a basis for us to create a game off of.

This repo in particular is the most up-to-date version. Our previous repo can be found here: [SOAP GOAP](https://bitbucket.org/soapgoap/imt3912-soap).

### What is GOAP? ###
GOAP, or Goal Oriented Action Planning, is a method by which we use *goals* as the foundation of what we want to accomplish, and we'll achieve it through a *plan* consisting of *actions*. This differs from the regular Finite-State-Machines in that you no longer move intrinsically linked states, but instead you choose a goal and formulate a plan that accomplishes the goal.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1\. Download all necessary programs and files:

* Visual Studio 2015
* Unreal Engine 4.15
* This project's zip, or clone it to a folder.

2\. Generate the project files.

* Enter the project folder, select SOAPGOAP.uproject.
* Open the GoapGame.uproject context menu. (I.e. right click the file.)
* Select "Generate Visual Studio project files".
* Wait for completion.
* Open GoapGame.sln.
* From the "Build" menu along the top bar, select "Build Solution"

3\. You can now launch the project by opening SOAPGOAP.uproject.

### How to use? ###

We recommend you read [Jeff Orkins paper](http://alumni.media.mit.edu/~jorkin/gdc2006_orkin_jeff_fear.pdf) or some other resource describing GOAP in order to familiarize yourself with the concepts of the algorithm. Understanding how actions, goals and conditions are used to produce plans should help greatly with getting started with our GOAP implementation.

Quick start guide for a new project.

After you have installed the modules you can start off by following these steps. 

1\. Create an action.

* Create a new blueprint class that inherits from GoapActionBase.
* Override the Execute function.
* Put whatever functionality the action should perform in the overriden Execute function.
* Determine where in the code the action has concluded succesfully, and call ApplyPostConditionsToBlackboard.
* Immediately following that, obtain a reference to the controller through the parent object and call ActorIsReady. Remember to cast the AIController to your derived controller in order to access the functions inherited from the GoapAIController. 

2\. Create conditions.

* Create a new blackboard.
* Determine what the preconditions and postconditions of your action are.
* Create Bool entries in the blackboard for those conditions and give them unique names. 
* On the details panel of the action blueprint, under the GOAP tab, create entries in the list of conditions with the names you used in the blackboard.
* Set the values for the conditions on the action blueprint. 

3\. Create your agent and controller.

* Create a new blueprint class that inherits from the GoapAIController.
* Under the GOAP tab, assign your blackboard to the controller.
* Place a character or pawn into the world. 
* Assign your controller to the agent.
* Assign your actions to the agent using the add component button. 

### Who do I talk to? ###

* Sveinung Tørresdal - sveinuto@stud.ntnu.no
* Jonas Askeland - jonaaske@stud.ntnu.no