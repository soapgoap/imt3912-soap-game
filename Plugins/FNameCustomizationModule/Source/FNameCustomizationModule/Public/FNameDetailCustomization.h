#pragma once
#include "FNameCustomizationModule.h"

#include "IPropertyTypeCustomization.h"

class FNameDetailCustomization : public IPropertyTypeCustomization
{
public:
	/**
	* Creates a new instance.
	*
	* @return A new customization for FName.
	*/
	static TSharedRef<IPropertyTypeCustomization> MakeInstance()
	{
		return MakeShareable(new FNameDetailCustomization());
	}

public:
	// IPropertyTypeCustomization interface
	virtual void CustomizeHeader(TSharedRef<IPropertyHandle> StructPropertyHandle, class FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;
	virtual void CustomizeChildren(TSharedRef<class IPropertyHandle> StructPropertyHandle, class IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;
	
private:
	//Enums
	enum FNameCondition
	{
		IsNew,
		Exists,
		NoMatch
	};

	//Functions
	FText	GetNameValue() const;
	void	UpdateValue(const FText& NewText);
	void	SetNameValue(const FText& NewText, ETextCommit::Type CommitInfo);

	FText	IsUnique() const;

private:
	//Variables
	TSharedPtr<IPropertyHandle> PropertyHandle;
	FNameCondition ThisName;
};