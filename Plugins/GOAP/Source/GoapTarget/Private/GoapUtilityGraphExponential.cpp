// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapUtilityGraphExponential.h"




float UGoapUtilityGraphExponential::Evaluate_Implementation()
{
	float Score = 0.0f;

	float Input = Blackboard->GetValueAsFloat(InputKey);

	Score = (FMath::Pow(Input, Exponent)) + Constant;

	return Score;
}