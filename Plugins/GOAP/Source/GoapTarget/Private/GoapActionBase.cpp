// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapActionBase.h"

void UGoapActionBase::Execute_Implementation()
{
	unimplemented()
}

bool UGoapActionBase::SetBlackboard(UBlackboardComponent *& ParentBlackboard)
{
	check(ParentBlackboard != nullptr);
	Blackboard = ParentBlackboard;

	return true;
}
/** Call this if the action was completed succesfully.
*/
void UGoapActionBase::ApplyPostConditionsToBlackboard()
{
	if (Blackboard != nullptr)
	{
		for (auto& Condition : PostConditions)
		{
			Blackboard->SetValueAsBool(Condition.Key, Condition.Value);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot apply post conditions because blackboard reference has not been set."));
	}
}
/** Call this if action failed to complete as intended and no longer fullfills the postconditions.
 * This is intended as a convenience, as updating the blackboard in blueprints from the component can be ugly. 
*/
void UGoapActionBase::ApplyFailureEffectsToBlackboard()
{
	if (Blackboard != nullptr)
	{
		for (auto& Condition : FailureEffects)
		{
			Blackboard->SetValueAsBool(Condition.Key, Condition.Value);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot failure effects because blackboard reference has not been set."));
	}
}
