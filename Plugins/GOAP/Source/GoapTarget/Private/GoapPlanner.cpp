// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapPlanner.h"



UGoapPlanner::UGoapPlanner()
{

}

void UGoapPlanner::SetBlackboard(UBlackboardComponent* &ParentBlackboard)
{
	check(ParentBlackboard != nullptr);
	
	Blackboard = ParentBlackboard;

}

void UGoapPlanner::SetAvailableActions(TArray<UGoapActionBase*>& ActionSet)
{
	AllActions.Reset(ActionSet.Num());
	for (auto& Action : ActionSet)
	{
		AllActions.Add(DuplicateObject<UGoapActionBase>(Action, this, NAME_None));
	}
	OriginalActions = ActionSet;
}

TArray<UGoapActionBase*> UGoapPlanner::MakeNewPlan(TMap<FName, bool> PlanEndGoal)
{
	TArray<std::shared_ptr<FGoapNode>> FrontierSet;
		
	TArray<UGoapActionBase*> CompleteActionPlan;

	if (Blackboard == nullptr || AllActions.Num() == 0)
	{
		return CompleteActionPlan;
	}
	// If any goal is unmet we have something to plan towards
	bool bGoalIsValid = false;
	for (auto& Goal : PlanEndGoal)
	{
		if (Goal.Value != Blackboard->GetValueAsBool(Goal.Key))
		{
			bGoalIsValid = true;
		}
	}
	if (bGoalIsValid == false)
	{
		return CompleteActionPlan;
	}

	// Make the initial frontier from actions that can satisfy the end goal
	TArray<UGoapActionBase*> AvailableActions;
	FindAllActionsThatSatisfyGoal(PlanEndGoal, AllActions, AvailableActions);
	for (auto& Action : AvailableActions)
	{
		std::shared_ptr<FGoapNode> NewNode = std::make_shared<FGoapNode>(Action, nullptr, 0);
		FrontierSet.Add(NewNode);
	}
	// If any of the preconditions in the frontier have already been met, remove them.
	for (auto& Node : FrontierSet)
	{
		for (auto& PreCondition : Node->Action->PreConditions)
		{
			if(PreCondition.Value == Blackboard->GetValueAsBool(PreCondition.Key))
			{
				Node->Action->PreConditions.FindAndRemoveChecked(PreCondition.Key);
			}
		}
	}
	

	uint8 MaxIterations = 25;
	uint8 Iterations = 0;
	bool bHaveFinishedPlanning = false;

	while (bHaveFinishedPlanning != true && Iterations < MaxIterations)
	{
		int16 IndexOfCheapestNode;
		IndexOfCheapestNode = GetCheapestIndex(FrontierSet);

		if (IndexOfCheapestNode < 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("GOAP ABORT: Negative index on cheapest node"));
			UE_LOG(LogTemp, Warning, TEXT("Dumping frontier after %d iterations:"), Iterations);
			for (auto& Node : FrontierSet)
			{
				UE_LOG(LogTemp, Warning, TEXT("\t%s"), *Node->Action->GetName());
			}
			break;
		}

		TMap<FName, bool> NextGoal = FrontierSet[IndexOfCheapestNode]->Action->PreConditions;
		TArray<UGoapActionBase*> AvailableActionsAtNode;
		FindAllActionsThatSatisfyGoal(NextGoal, AllActions, AvailableActionsAtNode);
		ExpandActions(FrontierSet, AvailableActionsAtNode, IndexOfCheapestNode);

		int16 CheapestValidStartIndex = GetCheapestStartIndex(FrontierSet);
		if (CheapestValidStartIndex >= 0)
		{
			CompleteActionPlan = GetFinalPlanActions(FrontierSet, CheapestValidStartIndex);
			bHaveFinishedPlanning = true;
		}

		FrontierSet[IndexOfCheapestNode]->Visited = true;

		Iterations++;
		if (Iterations == MaxIterations)
		{
			UE_LOG(LogTemp, Warning, TEXT("GOAP exceeded max iterations"));
			//for (auto& Node : FrontierSet)
			//{
			//	UE_LOG(LogTemp, Warning, TEXT("\t%s"), *Node->Action.GetName());
			//}
		}
	}

	// Manually freeing shared pointers
	for (uint8 i = 0; i < FrontierSet.Num(); i++)
	{
		FrontierSet[i].reset();
	}
	return CompleteActionPlan;
}

void UGoapPlanner::FindAllActionsThatSatisfyGoal(const TMap<FName, bool>& TargetGoal, TArray<UGoapActionBase*>& AvailableActions, TArray<UGoapActionBase*>& OutSet)
{
	for (auto& Action : AvailableActions)
	{
		bool bSatisfiesAGoal = false;
		for (auto Goal : TargetGoal)
		{
			bool* PostConditionValue = Action->PostConditions.Find(Goal.Key);

			if (PostConditionValue != nullptr)
			{
				if (*PostConditionValue == Goal.Value)
				{
					bSatisfiesAGoal = true;
				}
			}
		}
		if (bSatisfiesAGoal)
		{
			OutSet.Add(Action);
		}
	}
}


/** Find the node that is cheapest to explore, aka has the least unsatisfied preconditions
  * Can add in weights here eventually, or add different weights to different world state conditions?
*/
int16 UGoapPlanner::GetCheapestIndex(const TArray<std::shared_ptr<FGoapNode>>& Set)
{
	uint8 LowestCost = 255;
	int16 CheapestNodeIndex = -1;
	for (int32 i = 0; i < Set.Num(); i++)
	{
		if (Set[i]->Visited != true)
		{
			uint8 cost = Set[i]->Action->PreConditions.Num();
			cost += Set[i]->Depth;
			if (Set[i]->Action->PreConditions.Num() < LowestCost)
			{
				LowestCost = Set[i]->Action->PreConditions.Num();
				CheapestNodeIndex = i;
			}
		}
	}
	return CheapestNodeIndex;
}


/** Check if the preconditions of any of the nodes in the set can be entirely satisfied by the current world state
*/
int16 UGoapPlanner::GetCheapestStartIndex(const TArray<std::shared_ptr<FGoapNode>>& Set)
{
	uint8 CheapestValidStartCost = 255;
	int16 CheapestValidStartIndex = -1;
	for (int32 i = 0; i < Set.Num(); i++)
	{
		if (Set[i]->Visited != true)
		{
			bool bPreConditionsAreSatisfied = true;
			for (auto& PreCondition : Set[i]->Action->PreConditions)
			{
				if (PreCondition.Value != Blackboard->GetValueAsBool(PreCondition.Key))
				{
					bPreConditionsAreSatisfied = false;
				}
			}
			if (bPreConditionsAreSatisfied)
			{
				if (Set[i]->Action->PreConditions.Num() < CheapestValidStartCost)
				{
					CheapestValidStartCost = Set[i]->Action->PreConditions.Num();
					CheapestValidStartIndex = i;
				}
			}
		}
	}
	return CheapestValidStartIndex;
}


/** Follow parents from nodes to find what actions in the original list of actions to perform.
 * We get the actions form the original list because the action instances in the nodes have had their conditions adjusted.
*/
TArray<UGoapActionBase*> UGoapPlanner::GetFinalPlanActions(TArray<std::shared_ptr<FGoapNode>>& Set, int16 Index)
{
	TArray<UGoapActionBase*> Plan;
	if (Index >= 0)
	{
		std::shared_ptr<FGoapNode> CurrentParent = nullptr;
		int32 IndexOfAction = -1;
		for (int i = 0; i < OriginalActions.Num(); i++)
		{
			if (OriginalActions[i]->GetName() == Set[Index]->Action->GetName())
			{
				IndexOfAction = i;
			}
		}
		Plan.Add(OriginalActions[IndexOfAction]);
		CurrentParent = (Set[Index]->Parent);

		bool bFinished = false;
		while (!bFinished)
		{
			if (CurrentParent == nullptr)
			{
				bFinished = true;
			}
			else
			{
				int32 ActionIndex = -1;
				for (int i = 0; i < OriginalActions.Num(); i++)
				{
					if (OriginalActions[i]->GetName() == CurrentParent->Action->GetName())
					{
						ActionIndex = i;
					}
				}
				Plan.Add(OriginalActions[ActionIndex]);
				CurrentParent = CurrentParent->Parent;
			}
		}
	}
	return Plan;
}


/** Add all the actions to the set and remove satisfied preconditions
*/
void UGoapPlanner::ExpandActions(TArray<std::shared_ptr<FGoapNode>>& OutSet, TArray<UGoapActionBase*>& Actions, int16 Index)
{
	for (auto& Action : Actions)
	{
		uint8 Depth = OutSet[Index]->Depth + 1;

		std::shared_ptr<FGoapNode> NewNode = std::make_shared<FGoapNode>(Action, OutSet[Index], Depth);
		uint8 NewNodeIndex = OutSet.Add(NewNode);
		OutSet[NewNodeIndex]->Action->PreConditions.Append(OutSet[Index]->Action->PreConditions);

		// The new nodes satisfied some precondition from the previous node, so we remove those from the new nodes list of unsatisfied conditions
		for (auto& PostCondition : Action->PostConditions)
		{
			bool* PreConditionValuePtr = OutSet[Index]->Action->PreConditions.Find(PostCondition.Key);
			if (PreConditionValuePtr != nullptr)
			{
				if (*PreConditionValuePtr == PostCondition.Value)
				{
					OutSet[NewNodeIndex]->Action->PreConditions.FindAndRemoveChecked(PostCondition.Key);
				}
			}
		}
	}
}
