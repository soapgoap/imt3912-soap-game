// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"

#include "GoapAIController.h"



AGoapAIController::AGoapAIController()
{
	Blackboard = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));

	bReady = false;	//Wait for actor to report they are ready to receive actions
	TimeAccumulator = 0;
}

void AGoapAIController::BeginPlay()
{
	Super::BeginPlay();

	if (BlackboardDataAsset == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("No blackboard set for %s"), *GetName());
	}
	else
	{
		Blackboard->InitializeBlackboard(*BlackboardDataAsset);

		GoapPlanner = NewObject<UGoapPlanner>(this);
		GoapPlanner->SetBlackboard(Blackboard);

		GetPawn()->GetComponents<UGoapSensorBase>(SensorSet);

		GetPawn()->GetComponents<UGoapActionBase>(ActionSet);
		GoapPlanner->SetAvailableActions(ActionSet);

		GoalSelector = NewObject<UGoapUtilityEvaluator>(this);
		GoalSelector->SetBlackboard(Blackboard);

		for (auto& Action : ActionSet)
		{
			Action->SetBlackboard(Blackboard);
		}
		for (auto& Sensor : SensorSet)
		{
			Sensor->SetBlackboard(Blackboard);
		}
	}
}

void AGoapAIController::Tick(float DeltaTime)
{
	RunAllSensors();
	if (bReady) 
	{
		
		if (CheckPreconditionsAreSatisfied() == false)
		{
			MakeNewPlan();
		}
		CheckForBetterGoal();

		if (TryDoNextAction())
		{
			bReady = false;
		}
		else
		{
			if (MakeNewPlan())
			{
				bReady = true;
			}
		}
	}
}



void AGoapAIController::ActorReady()
{
	bReady = true;
}

void AGoapAIController::ActorInterrupt()
{
	bReady = false;
	//CurrentActionIndex = 0;
	CompleteActionPlan.Empty();
}

void AGoapAIController::SetGoal(TMap<FName, bool> GoalState)
{
	PlanEndGoal = GoalState;
}

/** Add a new set of goals. 
 * Wrapper due to lack of TMap support as parameter in blueprint callable function.
*/
void AGoapAIController::SetGoal(TArray<FName> Keys, TArray<bool> Values)
{
	int NumKeys = Keys.Num();
	if (NumKeys == Values.Num())
	{
		PlanEndGoal.Empty(NumKeys);
		for (int i = 0; i < NumKeys; i++)
		{
			PlanEndGoal.Add(Keys[i], Values[i]);
		}
	}
}

bool AGoapAIController::CheckPreconditionsAreSatisfied()
{
	bool bStillSatisfied = true;
	if (CompleteActionPlan.Num() > 0)
	{
		for (auto& PreCondition : CompleteActionPlan[0]->PreConditions)
		{
			bool Value = Blackboard->GetValueAsBool(PreCondition.Key);
			if (Value != PreCondition.Value)
			{
				bStillSatisfied = false;
			}
		}
	}
	return bStillSatisfied;
}

/** Check if the agent should change his goal. 
*/
void AGoapAIController::CheckForBetterGoal()
{
	if (GoalSelector->HaveGraphsToEvaluate())
	{
		TPair<bool, TMap<FName, bool>> NewGoal;
		NewGoal = GoalSelector->EvaluateAllGraphs();
		if (NewGoal.Key == true)
		{
			ActorInterrupt();
			SetGoal(NewGoal.Value);
			ActorReady();
			UE_LOG(LogTemp, Warning, TEXT("Goals have changed, goal is:"));
			for (auto& Goal : NewGoal.Value)
			{
				UE_LOG(LogTemp, Warning, TEXT("\t%s"), *Goal.Key.ToString());
			}
		}
	}
}

/** Perform the next GOAP action or start formulating a new plan if the previous plan has been exhausted.
*/
bool AGoapAIController::TryDoNextAction()
{
	if (CompleteActionPlan.Num() > 0)
	{
		bReady = false;
		UE_LOG(LogTemp, Warning, TEXT("Trying to execute action %s"), *CompleteActionPlan[CurrentActionIndex]->GetName());
		CompleteActionPlan[0]->Execute();
		CompleteActionPlan.RemoveAt(0);
		return true;
	}
	return false;
}
void AGoapAIController::RunAllSensors()
{
	for (auto& Sensor : SensorSet)
	{
		Sensor->RunSensor();
	}
}
bool AGoapAIController::MakeNewPlan()
{
	//GoapPlanner->SetAvailableActions(ActionSet);
	CompleteActionPlan = GoapPlanner->MakeNewPlan(PlanEndGoal);
	if (CompleteActionPlan.Num() > 0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Current plan is:"));
		//for (auto& Step : CompleteActionPlan)
		//{
		//	UE_LOG(LogTemp, Warning, TEXT("\t%s"), *Step->GetName());
		//}

		//UE_LOG(LogTemp, Warning, TEXT("Blackboard is:"));
		//for (auto& Key : BlackboardDataAsset->Keys)
		//{
		//	if (Key.KeyType->IsA(UBlackboardKeyType_Bool::StaticClass()))
		//	{
		//		bool value = Blackboard->GetValueAsBool(Key.EntryName);
		//		UE_LOG(LogTemp, Warning, TEXT("\t%s : %i"), *Key.EntryName.ToString(), value);
		//	}
		//}
		return true;
	}
	return false;
}