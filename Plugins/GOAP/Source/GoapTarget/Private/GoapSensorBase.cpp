// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTargetModule.h"
#include "GoapSensorBase.h"


void UGoapSensorBase::RunSensor_Implementation()
{
	unimplemented()
}

bool UGoapSensorBase::SetBlackboard(UBlackboardComponent *& ParentBlackboard)
{
	check(ParentBlackboard != nullptr);
	Blackboard = ParentBlackboard;

	return true;
}


void UGoapSensorBase::ApplySuccessEffectsToBlackboard()
{
	if (Blackboard != nullptr)
	{
		for (auto& Condition : SuccessEffects)
		{
			Blackboard->SetValueAsBool(Condition.Key, Condition.Value);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot apply success effects because blackboard reference has not been set."));
	}
}

void UGoapSensorBase::ApplyFailureEffectsToBlackboard()
{
	if (Blackboard != nullptr)
	{
		for (auto& Condition : FailureEffects)
		{
			Blackboard->SetValueAsBool(Condition.Key, Condition.Value);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot failure effects because blackboard reference has not been set."));
	}
}
