// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "UObject/NoExportTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GoapActionBase.generated.h"

/** Base class for all goap actions
 * 
 */
UCLASS(Blueprintable, meta = (BlueprintSpawnableComponent), ClassGroup = GOAP)
class GOAPTARGET_API UGoapActionBase : public UActorComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Execute();

	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> PreConditions;
	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> PostConditions;
	UPROPERTY(EditAnywhere, Category = "GOAP|Optional")
	TMap<FName, bool> FailureEffects;

	UFUNCTION(BlueprintCallable, Category = "GOAP|Action")
	bool SetBlackboard(UBlackboardComponent*& ParentBlackboard);
	UFUNCTION(BlueprintCallable, Category = "GOAP|Action")
	void ApplyPostConditionsToBlackboard();
	UFUNCTION(BlueprintCallable, Category = "GOAP|Action|Optional")
	void ApplyFailureEffectsToBlackboard();
protected:
	UBlackboardComponent* Blackboard;
};
