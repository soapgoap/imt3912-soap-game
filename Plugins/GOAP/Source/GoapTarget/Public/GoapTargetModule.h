// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GoapTargetModule.h"

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

class FGoapTargetModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};