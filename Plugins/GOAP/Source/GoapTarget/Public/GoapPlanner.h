// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "UObject/NoExportTypes.h"
#include <memory>
#include "GoapActionBase.h"
#include "GoapNode.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "GoapPlanner.generated.h"



/** This planner produces the sequence of actions that will satisfy the provided goal.
 *  You have to set the blackboard it should use and the actions it has to evaluate before calling MakeNewPlan.
 */
UCLASS()
class GOAPTARGET_API UGoapPlanner : public UObject
{
	GENERATED_BODY()
	UGoapPlanner();
	
public:
	TArray<UGoapActionBase*> MakeNewPlan(TMap<FName, bool> PlanEndGoal);

	void SetBlackboard(UBlackboardComponent*& ParentBlackboard);
	void SetAvailableActions(TArray<UGoapActionBase*>& ActionSet);


protected:
	UPROPERTY()
	UBlackboardComponent* Blackboard;
	TArray<UGoapActionBase*> AllActions;
	TArray<UGoapActionBase*> OriginalActions;

	void FindAllActionsThatSatisfyGoal(const TMap<FName, bool>& TargetGoal, TArray<UGoapActionBase*>& AvailableActions, TArray<UGoapActionBase*>& OutSet);
	int16 GetCheapestIndex(const TArray<std::shared_ptr<FGoapNode>>& Set);
	int16 GetCheapestStartIndex(const TArray<std::shared_ptr<FGoapNode>>& Set);
	TArray<UGoapActionBase*> GetFinalPlanActions(TArray<std::shared_ptr<FGoapNode>>& Set, int16 Index);
	void ExpandActions(TArray<std::shared_ptr<FGoapNode>>& OutSet, TArray<UGoapActionBase*>& Actions, int16 Index);
};
