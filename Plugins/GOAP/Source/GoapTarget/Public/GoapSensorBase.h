// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GoapTargetModule.h"
#include "Components/ActorComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GoapSensorBase.generated.h"


UCLASS(abstract, Blueprintable, meta = (BlueprintSpawnableComponent), ClassGroup = GOAP)
class GOAPTARGET_API UGoapSensorBase : public UActorComponent
{
	GENERATED_BODY()

public:	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void RunSensor();

	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> SuccessEffects;
	UPROPERTY(EditAnywhere, Category = "GOAP")
	TMap<FName, bool> FailureEffects;

	UFUNCTION(BlueprintCallable, Category = "GOAP|Sensor")
	bool SetBlackboard(UBlackboardComponent*& ParentBlackboard);
	UFUNCTION(BlueprintCallable, Category = "GOAP|Sensor")
	void ApplySuccessEffectsToBlackboard();
	UFUNCTION(BlueprintCallable, Category = "GOAP|Sensor")
	void ApplyFailureEffectsToBlackboard();
protected:
	UBlackboardComponent* Blackboard;
};
