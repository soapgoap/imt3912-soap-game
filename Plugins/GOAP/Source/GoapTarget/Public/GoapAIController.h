// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"
#include "GoapPlanner.h"
#include "GoapUtilityEvaluator.h"
#include "GoapActionBase.h"
#include "GoapSensorBase.h"
#include "GoapAIController.generated.h"


/**
 * Example base class for a complex goap agent.
 * This controller has a utility evaluator for selecting new goals.
 */
UCLASS()
class GOAPTARGET_API AGoapAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AGoapAIController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	

	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	void ActorReady();			//Actor is ready to receive work
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	void ActorInterrupt();		//Plan is invalid

protected:
	void SetGoal(TMap<FName, bool> GoalState);
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	void SetGoal(TArray<FName> Keys, TArray<bool> Values);
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	bool CheckPreconditionsAreSatisfied();
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	void CheckForBetterGoal();
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	bool TryDoNextAction();
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	void RunAllSensors();
	UFUNCTION(BlueprintCallable, Category = "GOAP|Controller")
	bool MakeNewPlan();

	UPROPERTY(EditAnywhere, Category="GOAP")
	UBlackboardData* BlackboardDataAsset;

	UPROPERTY()
	UGoapPlanner* GoapPlanner;

	UPROPERTY()
	UGoapUtilityEvaluator* GoalSelector;

	TArray<UGoapSensorBase*> SensorSet;
	TArray<UGoapActionBase*> ActionSet;
	UPROPERTY(BlueprintReadOnly, Category="GOAP|Controller")
	TArray<UGoapActionBase*> CompleteActionPlan;
private:
	bool bReady;
	uint8 CurrentActionIndex = 0;
	TMap<FName, bool> PlanEndGoal;
	float TimeAccumulator;
};
