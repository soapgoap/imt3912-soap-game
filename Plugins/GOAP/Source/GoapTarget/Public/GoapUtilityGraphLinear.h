// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "EngineMinimal.h"
#include "Object.h"
#include "GoapUtilityGraphBase.h"
#include "GoapUtilityGraphLinear.generated.h"

/**
 * y = m * x + c
 */
UCLASS()
class GOAPTARGET_API UGoapUtilityGraphLinear : public UGoapUtilityGraphBase
{
	GENERATED_BODY()
	
public:
	float Evaluate_Implementation();
	
	float Multiplier = 0;
	float Constant = 0;

	FName InputKey;
	
};
