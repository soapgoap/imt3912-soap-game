// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "EngineMinimal.h"
#include "Object.h"
#include "UObject/NoExportTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GoapUtilityGraphBase.generated.h"

/**
 * 
 */
UCLASS()
class GOAPTARGET_API UGoapUtilityGraphBase : public UObject
{
	GENERATED_BODY()
	
public:
	bool SetBlackboard(UBlackboardComponent*& ParentBlackboard);
	UFUNCTION(BlueprintNativeEvent, Category = "GOAP")
	float Evaluate();
	
	TMap<FName, bool> Goal;

protected:
	UPROPERTY()
	UBlackboardComponent* Blackboard;
};
