// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapTargetModule.h"

#include "EngineMinimal.h"
#include "Object.h"
#include "GoapUtilityGraphBase.h"
#include "GoapUtilityGraphExponential.generated.h"

/**
 * 
 */
UCLASS()
class GOAPTARGET_API UGoapUtilityGraphExponential : public UGoapUtilityGraphBase
{
	GENERATED_BODY()
	
public:
	float Evaluate_Implementation();

	float Exponent = 0;
	float Constant = 0;

	FName InputKey;
	
	
};
