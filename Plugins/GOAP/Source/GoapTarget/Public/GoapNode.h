#pragma once

#include "GoapTargetModule.h"

#include "Class.h"
#include <memory>
#include "GoapActionBase.h"
#include "GoapNode.generated.h"

USTRUCT()
struct FGoapNode
{
	GENERATED_USTRUCT_BODY()

	UGoapActionBase* Action;
	std::shared_ptr<FGoapNode> Parent;
	bool Visited;
	uint8 Depth;

	FGoapNode()
	{
		Visited = false;
		Depth = 0;
		Parent = nullptr;
	}

	FGoapNode(UGoapActionBase*& NewAction, std::shared_ptr<FGoapNode> NewParent, uint8 NewDepth)
	{
		Action = NewAction;
		Parent = NewParent;
		Visited = false;
		Depth = NewDepth;
	}

	bool operator ==(FGoapNode const*& rhs)
	{
		return(Action->GetName() == rhs->Action->GetName());
	}

};